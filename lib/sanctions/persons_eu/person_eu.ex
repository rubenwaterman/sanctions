defmodule Sanctions.PersonsEU.PersonEU do
  use Ecto.Schema
  import Ecto.Changeset

  schema "personseu" do
    field :identificationvalidfrom, :string
    field :identificationdiplomatic, :string
    field :namealiasmiddlename, :string
    field :birthdateyearrangeto, :string
    field :entityeureferencenumber, :string
    field :entitysubjecttype, :string
    field :addressregulationtype, :string
    field :entityregulationpublicationdate, :string
    field :addresscountryiso2code, :string
    field :identificationregulationorganisationtype, :string
    field :identificationvalidto, :string
    field :birthdateplace, :string
    field :addresslogicalid, :string
    field :identificationregulationpublicationurl, :string
    field :birthdateregulationtype, :string
    field :citizenshipregulationorganisationtype, :string
    field :namealiasregulationpublicationdate, :string
    field :entityregulationpublicationurl, :string
    field :birthdateregulationpublicationdate, :string
    field :birthdatezipcode, :string
    field :citizenshipregulationprogramme, :string
    field :birthdatecountryiso2code, :string
    field :namealiasregulationprogramme, :string
    field :citizenshipregulationpublicationurl, :string
    field :addressregulationprogramme, :string
    field :identificationrevokedbyissuer, :string
    field :namealiasfunction, :string
    field :addresspobox, :string
    field :identificationregulationtype, :string
    field :identificationtypecode, :string
    field :identificationknownfalse, :string
    field :namealiaslastname, :string
    field :citizenshipregulationtype, :string
    field :birthdateregulationnumbertitle, :string
    field :addressregulationnumbertitle, :string
    field :entityunitednationid, :string
    field :namealiasregulationlanguage, :string
    field :namealiaswholename, :string
    field :addresscity, :string
    field :identificationregulationprogramme, :string
    field :birthdateregion, :string
    field :addressstreet, :string
    field :citizenshipregulationlanguage, :string
    field :addressremark, :string
    field :identificationremark, :string
    field :identificationissueddate, :string
    field :citizenshipregion, :string
    field :entitysubjecttypeclassificationcode, :string
    field :namealiastitle, :string
    field :birthdateregulationentryintoforcedate, :string
    field :namealiasregulationpublicationurl, :string
    field :birthdateyear, :string
    field :addresszipcode, :string
    field :namealiasremark, :string
    field :namealiasnamelanguage, :string
    field :identificationreportedlost, :string
    field :birthdateregulationlanguage, :string
    field :citizenshipregulationpublicationdate, :string
    field :birthdatecalendartype, :string
    field :addresscountrydescription, :string
    field :entityregulationnumbertitle, :string
    field :birthdateyearrangefrom, :string
    field :birthdatecirca, :string
    field :identificationregion, :string
    field :citizenshipremark, :string
    field :identificationknownexpired, :string
    field :addressplace, :string
    field :birthdatecountrydescription, :string
    field :birthdatecity, :string
    field :birthdatemonth, :string
    field :namealiasgender, :string
    field :identificationregulationpublicationdate, :string
    field :identificationlatinnumber, :string
    field :birthdateregulationpublicationurl, :string
    field :identificationcountrydescription, :string
    field :birthdatelogicalid, :string
    field :namealiasregulationnumbertitle, :string
    field :identificationlogicalid, :string
    field :entityregulationentryintoforcedate, :string
    field :citizenshipcountrydescription, :string
    field :addressregulationpublicationurl, :string
    field :birthdateregulationprogramme, :string
    field :citizenshiplogicalid, :string
    field :identificationnameondocument, :string
    field :addressregulationorganisationtype, :string
    field :citizenshipregulationentryintoforcedate, :string
    field :citizenshipregulationnumbertitle, :string
    field :addresscontactinfo, :string
    field :addressregulationlanguage, :string
    field :birthdatebirthdate, :string
    field :birthdateregulationorganisationtype, :string
    field :identificationcountryiso2code, :string
    field :addressasatlistingtime, :string
    field :entityremark, :string
    field :entitydesignationdate, :string
    field :identificationregulationentryintoforcedate, :string
    field :birthdateremark, :string
    field :entityregulationorganisationtype, :string
    field :addressregion, :string
    field :namealiasregulationentryintoforcedate, :string
    field :namealiaslogicalid, :string
    field :identificationnumber, :string
    field :namealiasfirstname, :string
    field :birthdateday, :string
    field :identificationissuedby, :string
    field :addressregulationpublicationdate, :string
    field :entitydesignationdetails, :string
    field :entityregulationprogramme, :string
    field :addressregulationentryintoforcedate, :string
    field :identificationregulationnumbertitle, :string
    field :namealiasregulationorganisationtype, :string
    field :identificationtypedescription, :string
    field :filegenerationdate, :string
    field :entityregulationtype, :string
    field :citizenshipcountryiso2code, :string
    field :namealiasregulationtype, :string
    field :identificationregulationlanguage, :string
    field :entitylogicalid, :string

    timestamps()
  end

  @doc false
  def changeset(person_eu, attrs) do
    person_eu
    |> cast(attrs, [:filegenerationdate, :entitylogicalid, :entityeureferencenumber, :entityunitednationid, :entitydesignationdate, :entitydesignationdetails, :entityremark, :entitysubjecttype, :entitysubjecttypeclassificationcode, :entityregulationtype, :entityregulationorganisationtype, :entityregulationpublicationdate, :entityregulationentryintoforcedate, :entityregulationnumbertitle, :entityregulationprogramme, :entityregulationpublicationurl, :namealiaslastname, :namealiasfirstname, :namealiasmiddlename, :namealiaswholename, :namealiasnamelanguage, :namealiasgender, :namealiastitle, :namealiasfunction, :namealiaslogicalid, :namealiasregulationlanguage, :namealiasremark, :namealiasregulationtype, :namealiasregulationorganisationtype, :namealiasregulationpublicationdate, :namealiasregulationentryintoforcedate, :namealiasregulationnumbertitle, :namealiasregulationprogramme, :namealiasregulationpublicationurl, :addresscity, :addressstreet, :addresspobox, :addresszipcode, :addressregion, :addressplace, :addressasatlistingtime, :addresscontactinfo, :addresscountryiso2code, :addresscountrydescription, :addresslogicalid, :addressregulationlanguage, :addressremark, :addressregulationtype, :addressregulationorganisationtype, :addressregulationpublicationdate, :addressregulationentryintoforcedate, :addressregulationnumbertitle, :addressregulationprogramme, :addressregulationpublicationurl, :birthdatebirthdate, :birthdateday, :birthdatemonth, :birthdateyear, :birthdateyearrangefrom, :birthdateyearrangeto, :birthdatecirca, :birthdatecalendartype, :birthdatezipcode, :birthdateregion, :birthdateplace, :birthdatecity, :birthdatecountryiso2code, :birthdatecountrydescription, :birthdatelogicalid, :birthdateregulationlanguage, :birthdateremark, :birthdateregulationtype, :birthdateregulationorganisationtype, :birthdateregulationpublicationdate, :birthdateregulationentryintoforcedate, :birthdateregulationnumbertitle, :birthdateregulationprogramme, :birthdateregulationpublicationurl, :identificationnumber, :identificationdiplomatic, :identificationknownexpired, :identificationknownfalse, :identificationreportedlost, :identificationrevokedbyissuer, :identificationissuedby, :identificationissueddate, :identificationvalidfrom, :identificationvalidto, :identificationlatinnumber, :identificationnameondocument, :identificationtypecode, :identificationtypedescription, :identificationregion, :identificationcountryiso2code, :identificationcountrydescription, :identificationlogicalid, :identificationregulationlanguage, :identificationremark, :identificationregulationtype, :identificationregulationorganisationtype, :identificationregulationpublicationdate, :identificationregulationentryintoforcedate, :identificationregulationnumbertitle, :identificationregulationprogramme, :identificationregulationpublicationurl, :citizenshipregion, :citizenshipcountryiso2code, :citizenshipcountrydescription, :citizenshiplogicalid, :citizenshipregulationlanguage, :citizenshipremark, :citizenshipregulationtype, :citizenshipregulationorganisationtype, :citizenshipregulationpublicationdate, :citizenshipregulationentryintoforcedate, :citizenshipregulationnumbertitle, :citizenshipregulationprogramme, :citizenshipregulationpublicationurl])
    |> validate_required([:filegenerationdate, :entitylogicalid, :entityeureferencenumber, :entitysubjecttype, :entitysubjecttypeclassificationcode, :entityregulationtype, :entityregulationorganisationtype, :entityregulationpublicationdate, :entityregulationentryintoforcedate, :entityregulationnumbertitle, :entityregulationprogramme, :entityregulationpublicationurl])
  end
end
