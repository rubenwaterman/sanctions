defmodule Sanctions.PersonsEU do
  @moduledoc """
  The PersonsEU context.
  """

  import Ecto.Query, warn: false
  alias Sanctions.Repo

  alias Sanctions.PersonsEU.PersonEU

  @doc """
  Returns the list of personseu.

  ## Examples

      iex> list_personseu()
      [%PersonEU{}, ...]

  """
  def list_personseu do
    Repo.all(PersonEU)
  end

  def paginate_personseu(params \\ []) do
    PersonEU
    |> Repo.paginate(params)
  end

  @doc """
  Gets a single person_eu.

  Raises `Ecto.NoResultsError` if the Person eu does not exist.

  ## Examples

      iex> get_person_eu!(123)
      %PersonEU{}

      iex> get_person_eu!(456)
      ** (Ecto.NoResultsError)

  """
  def get_person_eu!(id), do: Repo.get!(PersonEU, id)

  @doc """
  Creates a person_eu.

  ## Examples

      iex> create_person_eu(%{field: value})
      {:ok, %PersonEU{}}

      iex> create_person_eu(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_person_eu(attrs \\ %{}) do
    %PersonEU{}
    |> PersonEU.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a person_eu.

  ## Examples

      iex> update_person_eu(person_eu, %{field: new_value})
      {:ok, %PersonEU{}}

      iex> update_person_eu(person_eu, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_person_eu(%PersonEU{} = person_eu, attrs) do
    person_eu
    |> PersonEU.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a person_eu.

  ## Examples

      iex> delete_person_eu(person_eu)
      {:ok, %PersonEU{}}

      iex> delete_person_eu(person_eu)
      {:error, %Ecto.Changeset{}}

  """
  def delete_person_eu(%PersonEU{} = person_eu) do
    Repo.delete(person_eu)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking person_eu changes.

  ## Examples

      iex> change_person_eu(person_eu)
      %Ecto.Changeset{data: %PersonEU{}}

  """
  def change_person_eu(%PersonEU{} = person_eu, attrs \\ %{}) do
    PersonEU.changeset(person_eu, attrs)
  end
end
