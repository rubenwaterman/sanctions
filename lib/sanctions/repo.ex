defmodule Sanctions.Repo do
  use Ecto.Repo,
    otp_app: :sanctions,
    adapter: Ecto.Adapters.Postgres
  use Scrivener, page_size: 10
end
