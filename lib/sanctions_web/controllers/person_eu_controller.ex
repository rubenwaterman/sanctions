defmodule SanctionsWeb.PersonEUController do
  use SanctionsWeb, :controller

  alias Sanctions.PersonsEU
  alias Sanctions.PersonsEU.PersonEU

  def index(conn, _params) do
    personseu = PersonsEU.list_personseu()
    render(conn, "index.html", personseu: personseu)
  end

  def new(conn, _params) do
    changeset = PersonsEU.change_person_eu(%PersonEU{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"person_eu" => person_eu_params}) do
    case PersonsEU.create_person_eu(person_eu_params) do
      {:ok, person_eu} ->
        conn
        |> put_flash(:info, "Person eu created successfully.")
        |> redirect(to: Routes.person_eu_path(conn, :show, person_eu))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    person_eu = PersonsEU.get_person_eu!(id)
    render(conn, "show.html", person_eu: person_eu)
  end

  def edit(conn, %{"id" => id}) do
    person_eu = PersonsEU.get_person_eu!(id)
    changeset = PersonsEU.change_person_eu(person_eu)
    render(conn, "edit.html", person_eu: person_eu, changeset: changeset)
  end

  def update(conn, %{"id" => id, "person_eu" => person_eu_params}) do
    person_eu = PersonsEU.get_person_eu!(id)

    case PersonsEU.update_person_eu(person_eu, person_eu_params) do
      {:ok, person_eu} ->
        conn
        |> put_flash(:info, "Person eu updated successfully.")
        |> redirect(to: Routes.person_eu_path(conn, :show, person_eu))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", person_eu: person_eu, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    person_eu = PersonsEU.get_person_eu!(id)
    {:ok, _person_eu} = PersonsEU.delete_person_eu(person_eu)

    conn
    |> put_flash(:info, "Person eu deleted successfully.")
    |> redirect(to: Routes.person_eu_path(conn, :index))
  end
end
