defmodule SanctionsWeb.CountryView do
  use SanctionsWeb, :view

  alias Cldr.Territory

  def country_select(form, opts \\ []) do
    select(form, :address_country, country_options(), opts)
  end

  def country_options() do
    Territory.country_codes()
    |> Enum.map(&country_option/1)
    |> Enum.sort(&(&1[:key] <= &2[:key]))
  end

  def country_option(country) do
    [key: Territory.from_territory_code!(country, Sanctions.Cldr),
     value: country,
     data_label: Territory.to_unicode_flag!(country)]
  end
end
