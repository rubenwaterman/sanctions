defmodule SanctionsWeb.PersonEUListLive do
  use Phoenix.LiveView

  alias SanctionsWeb.Router.Helpers, as: Routes
  alias SanctionsWeb.PersonEUListLive

  alias Sanctions.PersonsEU

  def mount(_params, _session, socket) do
    %{entries: entries, page_number: page_number, page_size: page_size, total_entries: total_entries, total_pages: total_pages} =
      if connected?(socket)  do
        PersonsEU.paginate_personseu
      else
        %Scrivener.Page{}
      end

    assigns = [
      conn: socket,
      personseu: entries,
      page_number: page_number || 0,
      page_size: page_size || 0,
      total_entries: total_entries || 0,
      total_pages: total_pages || 0
    ]

    {:ok, assign(socket, assigns)}
  end

  def render(assigns) do
    SanctionsWeb.PersonEUView.render("personseu.html", assigns)
  end

  def handle_event("nav", %{"page" => page}, _url, socket) do
    {:noreply, push_redirect(socket, to: Routes.live_path(socket, PersonEUListLive, page: page))}
  end
  def handle_params(%{"page" => page}, _, socket) do
    assigns = get_and_assign_page(page)
    {:noreply, assign(socket, assigns)}
  end

  def handle_params(_, _, socket) do
    assigns = get_and_assign_page(nil)
    {:noreply, assign(socket, assigns)}
  end

  def get_and_assign_page(page_number) do
    %{
      entries: entries,
      page_number: page_number,
      page_size: page_size,
      total_entries: total_entries,
      total_pages: total_pages
    } = PersonsEU.paginate_personseu(page: page_number)

    [
      eupersons: entries,
      page_number: page_number,
      page_size: page_size,
      total_entries: total_entries,
      total_pages: total_pages
    ]
  end

end
