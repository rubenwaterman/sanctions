defmodule SanctionsWeb.LiveHelpers do
  import Phoenix.LiveView.Helpers

  @doc """
  Renders a component inside the `SanctionsWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, SanctionsWeb.PersonLive.FormComponent,
        id: @person.id || :new,
        action: @live_action,
        person: @person,
        return_to: Routes.person_index_path(@socket, :index) %>
  """
  def live_modal(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    title = Keyword.fetch!(opts, :title)
    modal_opts = [id: :modal, return_to: path, title: title, component: component, opts: opts]
    live_component(socket, SanctionsWeb.ModalComponent, modal_opts)
  end
end
