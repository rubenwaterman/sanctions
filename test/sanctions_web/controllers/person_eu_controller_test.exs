defmodule SanctionsWeb.PersonEUControllerTest do
  use SanctionsWeb.ConnCase

  alias Sanctions.PersonsEU

  @create_attrs %{entitylogicalid: "some entitylogicalid", identificationregulationlanguage: "some identificationregulationlanguage", namealiasregulationtype: "some namealiasregulationtype", citizenshipcountryiso2code: "some citizenshipcountryiso2code", entityregulationtype: "some entityregulationtype", filegenerationdate: "some filegenerationdate", identificationtypedescription: "some identificationtypedescription", namealiasregulationorganisationtype: "some namealiasregulationorganisationtype", identificationregulationnumbertitle: "some identificationregulationnumbertitle", addressregulationentryintoforcedate: "some addressregulationentryintoforcedate", entityregulationprogramme: "some entityregulationprogramme", entitydesignationdetails: "some entitydesignationdetails", addressregulationpublicationdate: "some addressregulationpublicationdate", identificationissuedby: "some identificationissuedby", birthdateday: "some birthdateday", namealiasfirstname: "some namealiasfirstname", identificationnumber: "some identificationnumber", namealiaslogicalid: "some namealiaslogicalid", namealiasregulationentryintoforcedate: "some namealiasregulationentryintoforcedate", addressregion: "some addressregion", entityregulationorganisationtype: "some entityregulationorganisationtype", birthdateremark: "some birthdateremark", identificationregulationentryintoforcedate: "some identificationregulationentryintoforcedate", entitydesignationdate: "some entitydesignationdate", entityremark: "some entityremark", addressasatlistingtime: "some addressasatlistingtime", identificationcountryiso2code: "some identificationcountryiso2code", birthdateregulationorganisationtype: "some birthdateregulationorganisationtype", birthdatebirthdate: "some birthdatebirthdate", addressregulationlanguage: "some addressregulationlanguage", addresscontactinfo: "some addresscontactinfo", citizenshipregulationnumbertitle: "some citizenshipregulationnumbertitle", citizenshipregulationentryintoforcedate: "some citizenshipregulationentryintoforcedate", addressregulationorganisationtype: "some addressregulationorganisationtype", identificationnameondocument: "some identificationnameondocument", citizenshiplogicalid: "some citizenshiplogicalid", birthdateregulationprogramme: "some birthdateregulationprogramme", addressregulationpublicationurl: "some addressregulationpublicationurl", citizenshipcountrydescription: "some citizenshipcountrydescription", entityregulationentryintoforcedate: "some entityregulationentryintoforcedate", identificationlogicalid: "some identificationlogicalid", namealiasregulationnumbertitle: "some namealiasregulationnumbertitle", birthdatelogicalid: "some birthdatelogicalid", identificationcountrydescription: "some identificationcountrydescription", birthdateregulationpublicationurl: "some birthdateregulationpublicationurl", identificationlatinnumber: "some identificationlatinnumber", identificationregulationpublicationdate: "some identificationregulationpublicationdate", namealiasgender: "some namealiasgender", birthdatemonth: "some birthdatemonth", birthdatecity: "some birthdatecity", ...}
  @update_attrs %{entitylogicalid: "some updated entitylogicalid", identificationregulationlanguage: "some updated identificationregulationlanguage", namealiasregulationtype: "some updated namealiasregulationtype", citizenshipcountryiso2code: "some updated citizenshipcountryiso2code", entityregulationtype: "some updated entityregulationtype", filegenerationdate: "some updated filegenerationdate", identificationtypedescription: "some updated identificationtypedescription", namealiasregulationorganisationtype: "some updated namealiasregulationorganisationtype", identificationregulationnumbertitle: "some updated identificationregulationnumbertitle", addressregulationentryintoforcedate: "some updated addressregulationentryintoforcedate", entityregulationprogramme: "some updated entityregulationprogramme", entitydesignationdetails: "some updated entitydesignationdetails", addressregulationpublicationdate: "some updated addressregulationpublicationdate", identificationissuedby: "some updated identificationissuedby", birthdateday: "some updated birthdateday", namealiasfirstname: "some updated namealiasfirstname", identificationnumber: "some updated identificationnumber", namealiaslogicalid: "some updated namealiaslogicalid", namealiasregulationentryintoforcedate: "some updated namealiasregulationentryintoforcedate", addressregion: "some updated addressregion", entityregulationorganisationtype: "some updated entityregulationorganisationtype", birthdateremark: "some updated birthdateremark", identificationregulationentryintoforcedate: "some updated identificationregulationentryintoforcedate", entitydesignationdate: "some updated entitydesignationdate", entityremark: "some updated entityremark", addressasatlistingtime: "some updated addressasatlistingtime", identificationcountryiso2code: "some updated identificationcountryiso2code", birthdateregulationorganisationtype: "some updated birthdateregulationorganisationtype", birthdatebirthdate: "some updated birthdatebirthdate", addressregulationlanguage: "some updated addressregulationlanguage", addresscontactinfo: "some updated addresscontactinfo", citizenshipregulationnumbertitle: "some updated citizenshipregulationnumbertitle", citizenshipregulationentryintoforcedate: "some updated citizenshipregulationentryintoforcedate", addressregulationorganisationtype: "some updated addressregulationorganisationtype", identificationnameondocument: "some updated identificationnameondocument", citizenshiplogicalid: "some updated citizenshiplogicalid", birthdateregulationprogramme: "some updated birthdateregulationprogramme", addressregulationpublicationurl: "some updated addressregulationpublicationurl", citizenshipcountrydescription: "some updated citizenshipcountrydescription", entityregulationentryintoforcedate: "some updated entityregulationentryintoforcedate", identificationlogicalid: "some updated identificationlogicalid", namealiasregulationnumbertitle: "some updated namealiasregulationnumbertitle", birthdatelogicalid: "some updated birthdatelogicalid", identificationcountrydescription: "some updated identificationcountrydescription", birthdateregulationpublicationurl: "some updated birthdateregulationpublicationurl", identificationlatinnumber: "some updated identificationlatinnumber", identificationregulationpublicationdate: "some updated identificationregulationpublicationdate", namealiasgender: "some updated namealiasgender", birthdatemonth: "some updated birthdatemonth", birthdatecity: "some updated birthdatecity", ...}
  @invalid_attrs %{entitylogicalid: nil, identificationregulationlanguage: nil, namealiasregulationtype: nil, citizenshipcountryiso2code: nil, entityregulationtype: nil, filegenerationdate: nil, identificationtypedescription: nil, namealiasregulationorganisationtype: nil, identificationregulationnumbertitle: nil, addressregulationentryintoforcedate: nil, entityregulationprogramme: nil, entitydesignationdetails: nil, addressregulationpublicationdate: nil, identificationissuedby: nil, birthdateday: nil, namealiasfirstname: nil, identificationnumber: nil, namealiaslogicalid: nil, namealiasregulationentryintoforcedate: nil, addressregion: nil, entityregulationorganisationtype: nil, birthdateremark: nil, identificationregulationentryintoforcedate: nil, entitydesignationdate: nil, entityremark: nil, addressasatlistingtime: nil, identificationcountryiso2code: nil, birthdateregulationorganisationtype: nil, birthdatebirthdate: nil, addressregulationlanguage: nil, addresscontactinfo: nil, citizenshipregulationnumbertitle: nil, citizenshipregulationentryintoforcedate: nil, addressregulationorganisationtype: nil, identificationnameondocument: nil, citizenshiplogicalid: nil, birthdateregulationprogramme: nil, addressregulationpublicationurl: nil, citizenshipcountrydescription: nil, entityregulationentryintoforcedate: nil, identificationlogicalid: nil, namealiasregulationnumbertitle: nil, birthdatelogicalid: nil, identificationcountrydescription: nil, birthdateregulationpublicationurl: nil, identificationlatinnumber: nil, identificationregulationpublicationdate: nil, namealiasgender: nil, birthdatemonth: nil, birthdatecity: nil, ...}

  def fixture(:person_eu) do
    {:ok, person_eu} = PersonsEU.create_person_eu(@create_attrs)
    person_eu
  end

  describe "index" do
    test "lists all personseu", %{conn: conn} do
      conn = get(conn, Routes.person_eu_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Personseu"
    end
  end

  describe "new person_eu" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.person_eu_path(conn, :new))
      assert html_response(conn, 200) =~ "New Person eu"
    end
  end

  describe "create person_eu" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.person_eu_path(conn, :create), person_eu: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.person_eu_path(conn, :show, id)

      conn = get(conn, Routes.person_eu_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Person eu"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.person_eu_path(conn, :create), person_eu: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Person eu"
    end
  end

  describe "edit person_eu" do
    setup [:create_person_eu]

    test "renders form for editing chosen person_eu", %{conn: conn, person_eu: person_eu} do
      conn = get(conn, Routes.person_eu_path(conn, :edit, person_eu))
      assert html_response(conn, 200) =~ "Edit Person eu"
    end
  end

  describe "update person_eu" do
    setup [:create_person_eu]

    test "redirects when data is valid", %{conn: conn, person_eu: person_eu} do
      conn = put(conn, Routes.person_eu_path(conn, :update, person_eu), person_eu: @update_attrs)
      assert redirected_to(conn) == Routes.person_eu_path(conn, :show, person_eu)

      conn = get(conn, Routes.person_eu_path(conn, :show, person_eu))
      assert html_response(conn, 200) =~ "some updated entitylogicalid"
    end

    test "renders errors when data is invalid", %{conn: conn, person_eu: person_eu} do
      conn = put(conn, Routes.person_eu_path(conn, :update, person_eu), person_eu: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Person eu"
    end
  end

  describe "delete person_eu" do
    setup [:create_person_eu]

    test "deletes chosen person_eu", %{conn: conn, person_eu: person_eu} do
      conn = delete(conn, Routes.person_eu_path(conn, :delete, person_eu))
      assert redirected_to(conn) == Routes.person_eu_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.person_eu_path(conn, :show, person_eu))
      end
    end
  end

  defp create_person_eu(_) do
    person_eu = fixture(:person_eu)
    %{person_eu: person_eu}
  end
end
