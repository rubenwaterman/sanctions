defmodule SanctionsWeb.PersonLiveTest do
  use SanctionsWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Sanctions.Persons

  @create_attrs %{identificationnameondocument: "some identificationnameondocument", namealiaslastname: "some namealiaslastname", addressasatlistingtime: "some addressasatlistingtime", birthdateregulationprogramme: "some birthdateregulationprogramme", addressregulationpublicationdate: "some addressregulationpublicationdate", entityremark: "some entityremark", identificationissuedby: "some identificationissuedby", birthdateyearrangeto: "some birthdateyearrangeto", addresscountrydescription: "some addresscountrydescription", identificationissueddate: "some identificationissueddate", birthdatecountrydescription: "some birthdatecountrydescription", birthdateregulationpublicationurl: "some birthdateregulationpublicationurl", birthdateregulationentryintoforcedate: "some birthdateregulationentryintoforcedate", addressregulationlanguage: "some addressregulationlanguage", addressstreet: "some addressstreet", identificationtypedescription: "some identificationtypedescription", namealiastitle: "some namealiastitle", addressregulationtype: "some addressregulationtype", citizenshipregulationentryintoforcedate: "some citizenshipregulationentryintoforcedate", identificationlatinnumber: "some identificationlatinnumber", addressremark: "some addressremark", birthdatecirca: "some birthdatecirca", identificationtypecode: "some identificationtypecode", citizenshipremark: "some citizenshipremark", namealiasregulationpublicationdate: "some namealiasregulationpublicationdate", citizenshipregulationorganisationtype: "some citizenshipregulationorganisationtype", namealiasfunction: "some namealiasfunction", identificationdiplomatic: "some identificationdiplomatic", citizenshipregulationtype: "some citizenshipregulationtype", entityregulationpublicationurl: "some entityregulationpublicationurl", identificationregulationprogramme: "some identificationregulationprogramme", identificationregulationpublicationdate: "some identificationregulationpublicationdate", filegenerationdate: "some filegenerationdate", identificationvalidfrom: "some identificationvalidfrom", entityeureferencenumber: "some entityeureferencenumber", namealiaslogicalid: "some namealiaslogicalid", addressregulationentryintoforcedate: "some addressregulationentryintoforcedate", birthdateregulationorganisationtype: "some birthdateregulationorganisationtype", citizenshiplogicalid: "some citizenshiplogicalid", identificationcountryiso2code: "some identificationcountryiso2code", identificationnumber: "some identificationnumber", addressregulationnumbertitle: "some addressregulationnumbertitle", citizenshipregulationpublicationurl: "some citizenshipregulationpublicationurl", birthdateyear: "some birthdateyear", birthdateyearrangefrom: "some birthdateyearrangefrom", entityregulationtype: "some entityregulationtype", identificationregion: "some identificationregion", entityunitednationid: "some entityunitednationid", birthdatemonth: "some birthdatemonth", namealiasmiddlename: "some namealiasmiddlename", ...}
  @update_attrs %{identificationnameondocument: "some updated identificationnameondocument", namealiaslastname: "some updated namealiaslastname", addressasatlistingtime: "some updated addressasatlistingtime", birthdateregulationprogramme: "some updated birthdateregulationprogramme", addressregulationpublicationdate: "some updated addressregulationpublicationdate", entityremark: "some updated entityremark", identificationissuedby: "some updated identificationissuedby", birthdateyearrangeto: "some updated birthdateyearrangeto", addresscountrydescription: "some updated addresscountrydescription", identificationissueddate: "some updated identificationissueddate", birthdatecountrydescription: "some updated birthdatecountrydescription", birthdateregulationpublicationurl: "some updated birthdateregulationpublicationurl", birthdateregulationentryintoforcedate: "some updated birthdateregulationentryintoforcedate", addressregulationlanguage: "some updated addressregulationlanguage", addressstreet: "some updated addressstreet", identificationtypedescription: "some updated identificationtypedescription", namealiastitle: "some updated namealiastitle", addressregulationtype: "some updated addressregulationtype", citizenshipregulationentryintoforcedate: "some updated citizenshipregulationentryintoforcedate", identificationlatinnumber: "some updated identificationlatinnumber", addressremark: "some updated addressremark", birthdatecirca: "some updated birthdatecirca", identificationtypecode: "some updated identificationtypecode", citizenshipremark: "some updated citizenshipremark", namealiasregulationpublicationdate: "some updated namealiasregulationpublicationdate", citizenshipregulationorganisationtype: "some updated citizenshipregulationorganisationtype", namealiasfunction: "some updated namealiasfunction", identificationdiplomatic: "some updated identificationdiplomatic", citizenshipregulationtype: "some updated citizenshipregulationtype", entityregulationpublicationurl: "some updated entityregulationpublicationurl", identificationregulationprogramme: "some updated identificationregulationprogramme", identificationregulationpublicationdate: "some updated identificationregulationpublicationdate", filegenerationdate: "some updated filegenerationdate", identificationvalidfrom: "some updated identificationvalidfrom", entityeureferencenumber: "some updated entityeureferencenumber", namealiaslogicalid: "some updated namealiaslogicalid", addressregulationentryintoforcedate: "some updated addressregulationentryintoforcedate", birthdateregulationorganisationtype: "some updated birthdateregulationorganisationtype", citizenshiplogicalid: "some updated citizenshiplogicalid", identificationcountryiso2code: "some updated identificationcountryiso2code", identificationnumber: "some updated identificationnumber", addressregulationnumbertitle: "some updated addressregulationnumbertitle", citizenshipregulationpublicationurl: "some updated citizenshipregulationpublicationurl", birthdateyear: "some updated birthdateyear", birthdateyearrangefrom: "some updated birthdateyearrangefrom", entityregulationtype: "some updated entityregulationtype", identificationregion: "some updated identificationregion", entityunitednationid: "some updated entityunitednationid", birthdatemonth: "some updated birthdatemonth", namealiasmiddlename: "some updated namealiasmiddlename", ...}
  @invalid_attrs %{identificationnameondocument: nil, namealiaslastname: nil, addressasatlistingtime: nil, birthdateregulationprogramme: nil, addressregulationpublicationdate: nil, entityremark: nil, identificationissuedby: nil, birthdateyearrangeto: nil, addresscountrydescription: nil, identificationissueddate: nil, birthdatecountrydescription: nil, birthdateregulationpublicationurl: nil, birthdateregulationentryintoforcedate: nil, addressregulationlanguage: nil, addressstreet: nil, identificationtypedescription: nil, namealiastitle: nil, addressregulationtype: nil, citizenshipregulationentryintoforcedate: nil, identificationlatinnumber: nil, addressremark: nil, birthdatecirca: nil, identificationtypecode: nil, citizenshipremark: nil, namealiasregulationpublicationdate: nil, citizenshipregulationorganisationtype: nil, namealiasfunction: nil, identificationdiplomatic: nil, citizenshipregulationtype: nil, entityregulationpublicationurl: nil, identificationregulationprogramme: nil, identificationregulationpublicationdate: nil, filegenerationdate: nil, identificationvalidfrom: nil, entityeureferencenumber: nil, namealiaslogicalid: nil, addressregulationentryintoforcedate: nil, birthdateregulationorganisationtype: nil, citizenshiplogicalid: nil, identificationcountryiso2code: nil, identificationnumber: nil, addressregulationnumbertitle: nil, citizenshipregulationpublicationurl: nil, birthdateyear: nil, birthdateyearrangefrom: nil, entityregulationtype: nil, identificationregion: nil, entityunitednationid: nil, birthdatemonth: nil, namealiasmiddlename: nil, ...}

  defp fixture(:person) do
    {:ok, person} = Persons.create_person(@create_attrs)
    person
  end

  defp create_person(_) do
    person = fixture(:person)
    %{person: person}
  end

  describe "Index" do
    setup [:create_person]

    test "lists all persons", %{conn: conn, person: person} do
      {:ok, _index_live, html} = live(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Listing Persons"
      assert html =~ person.identificationnameondocument
    end

    test "saves new person", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("a[href=\"/persons/new\"]") |> render_click() =~
               "New Person"

      assert_patch(index_live, Routes.person_index_path(conn, :new))

      assert index_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#person-form", person: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Person created successfully"
      assert html =~ "some identificationnameondocument"
    end

    test "updates person in listing", %{conn: conn, person: person} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("#person-#{person.id} a", "Edit") |> render_click() =~
               "Edit Person"

      assert_patch(index_live, Routes.person_index_path(conn, :edit, person))

      assert index_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#person-form", person: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Person updated successfully"
      assert html =~ "some updated identificationnameondocument"
    end

    test "deletes person in listing", %{conn: conn, person: person} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("#person-#{person.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#person-#{person.id}")
    end
  end

  describe "Show" do
    setup [:create_person]

    test "displays person", %{conn: conn, person: person} do
      {:ok, _show_live, html} = live(conn, Routes.person_show_path(conn, :show, person))

      assert html =~ "Show Person"
      assert html =~ person.identificationnameondocument
    end

    test "updates person within modal", %{conn: conn, person: person} do
      {:ok, show_live, _html} = live(conn, Routes.person_show_path(conn, :show, person))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Person"

      assert_patch(show_live, Routes.person_show_path(conn, :edit, person))

      assert show_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#person-form", person: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_show_path(conn, :show, person))

      assert html =~ "Person updated successfully"
      assert html =~ "some updated identificationnameondocument"
    end
  end
end
