defmodule Sanctions.PersonsTest do
  use Sanctions.DataCase

  alias Sanctions.Persons

  describe "persons" do
    alias Sanctions.Persons.Person

    @valid_attrs %{identificationnameondocument: "some identificationnameondocument", namealiaslastname: "some namealiaslastname", addressasatlistingtime: "some addressasatlistingtime", birthdateregulationprogramme: "some birthdateregulationprogramme", addressregulationpublicationdate: "some addressregulationpublicationdate", entityremark: "some entityremark", identificationissuedby: "some identificationissuedby", birthdateyearrangeto: "some birthdateyearrangeto", addresscountrydescription: "some addresscountrydescription", identificationissueddate: "some identificationissueddate", birthdatecountrydescription: "some birthdatecountrydescription", birthdateregulationpublicationurl: "some birthdateregulationpublicationurl", birthdateregulationentryintoforcedate: "some birthdateregulationentryintoforcedate", addressregulationlanguage: "some addressregulationlanguage", addressstreet: "some addressstreet", identificationtypedescription: "some identificationtypedescription", namealiastitle: "some namealiastitle", addressregulationtype: "some addressregulationtype", citizenshipregulationentryintoforcedate: "some citizenshipregulationentryintoforcedate", identificationlatinnumber: "some identificationlatinnumber", addressremark: "some addressremark", birthdatecirca: "some birthdatecirca", identificationtypecode: "some identificationtypecode", citizenshipremark: "some citizenshipremark", namealiasregulationpublicationdate: "some namealiasregulationpublicationdate", citizenshipregulationorganisationtype: "some citizenshipregulationorganisationtype", namealiasfunction: "some namealiasfunction", identificationdiplomatic: "some identificationdiplomatic", citizenshipregulationtype: "some citizenshipregulationtype", entityregulationpublicationurl: "some entityregulationpublicationurl", identificationregulationprogramme: "some identificationregulationprogramme", identificationregulationpublicationdate: "some identificationregulationpublicationdate", filegenerationdate: "some filegenerationdate", identificationvalidfrom: "some identificationvalidfrom", entityeureferencenumber: "some entityeureferencenumber", namealiaslogicalid: "some namealiaslogicalid", addressregulationentryintoforcedate: "some addressregulationentryintoforcedate", birthdateregulationorganisationtype: "some birthdateregulationorganisationtype", citizenshiplogicalid: "some citizenshiplogicalid", identificationcountryiso2code: "some identificationcountryiso2code", identificationnumber: "some identificationnumber", addressregulationnumbertitle: "some addressregulationnumbertitle", citizenshipregulationpublicationurl: "some citizenshipregulationpublicationurl", birthdateyear: "some birthdateyear", birthdateyearrangefrom: "some birthdateyearrangefrom", entityregulationtype: "some entityregulationtype", identificationregion: "some identificationregion", entityunitednationid: "some entityunitednationid", birthdatemonth: "some birthdatemonth", namealiasmiddlename: "some namealiasmiddlename", ...}
    @update_attrs %{identificationnameondocument: "some updated identificationnameondocument", namealiaslastname: "some updated namealiaslastname", addressasatlistingtime: "some updated addressasatlistingtime", birthdateregulationprogramme: "some updated birthdateregulationprogramme", addressregulationpublicationdate: "some updated addressregulationpublicationdate", entityremark: "some updated entityremark", identificationissuedby: "some updated identificationissuedby", birthdateyearrangeto: "some updated birthdateyearrangeto", addresscountrydescription: "some updated addresscountrydescription", identificationissueddate: "some updated identificationissueddate", birthdatecountrydescription: "some updated birthdatecountrydescription", birthdateregulationpublicationurl: "some updated birthdateregulationpublicationurl", birthdateregulationentryintoforcedate: "some updated birthdateregulationentryintoforcedate", addressregulationlanguage: "some updated addressregulationlanguage", addressstreet: "some updated addressstreet", identificationtypedescription: "some updated identificationtypedescription", namealiastitle: "some updated namealiastitle", addressregulationtype: "some updated addressregulationtype", citizenshipregulationentryintoforcedate: "some updated citizenshipregulationentryintoforcedate", identificationlatinnumber: "some updated identificationlatinnumber", addressremark: "some updated addressremark", birthdatecirca: "some updated birthdatecirca", identificationtypecode: "some updated identificationtypecode", citizenshipremark: "some updated citizenshipremark", namealiasregulationpublicationdate: "some updated namealiasregulationpublicationdate", citizenshipregulationorganisationtype: "some updated citizenshipregulationorganisationtype", namealiasfunction: "some updated namealiasfunction", identificationdiplomatic: "some updated identificationdiplomatic", citizenshipregulationtype: "some updated citizenshipregulationtype", entityregulationpublicationurl: "some updated entityregulationpublicationurl", identificationregulationprogramme: "some updated identificationregulationprogramme", identificationregulationpublicationdate: "some updated identificationregulationpublicationdate", filegenerationdate: "some updated filegenerationdate", identificationvalidfrom: "some updated identificationvalidfrom", entityeureferencenumber: "some updated entityeureferencenumber", namealiaslogicalid: "some updated namealiaslogicalid", addressregulationentryintoforcedate: "some updated addressregulationentryintoforcedate", birthdateregulationorganisationtype: "some updated birthdateregulationorganisationtype", citizenshiplogicalid: "some updated citizenshiplogicalid", identificationcountryiso2code: "some updated identificationcountryiso2code", identificationnumber: "some updated identificationnumber", addressregulationnumbertitle: "some updated addressregulationnumbertitle", citizenshipregulationpublicationurl: "some updated citizenshipregulationpublicationurl", birthdateyear: "some updated birthdateyear", birthdateyearrangefrom: "some updated birthdateyearrangefrom", entityregulationtype: "some updated entityregulationtype", identificationregion: "some updated identificationregion", entityunitednationid: "some updated entityunitednationid", birthdatemonth: "some updated birthdatemonth", namealiasmiddlename: "some updated namealiasmiddlename", ...}
    @invalid_attrs %{identificationnameondocument: nil, namealiaslastname: nil, addressasatlistingtime: nil, birthdateregulationprogramme: nil, addressregulationpublicationdate: nil, entityremark: nil, identificationissuedby: nil, birthdateyearrangeto: nil, addresscountrydescription: nil, identificationissueddate: nil, birthdatecountrydescription: nil, birthdateregulationpublicationurl: nil, birthdateregulationentryintoforcedate: nil, addressregulationlanguage: nil, addressstreet: nil, identificationtypedescription: nil, namealiastitle: nil, addressregulationtype: nil, citizenshipregulationentryintoforcedate: nil, identificationlatinnumber: nil, addressremark: nil, birthdatecirca: nil, identificationtypecode: nil, citizenshipremark: nil, namealiasregulationpublicationdate: nil, citizenshipregulationorganisationtype: nil, namealiasfunction: nil, identificationdiplomatic: nil, citizenshipregulationtype: nil, entityregulationpublicationurl: nil, identificationregulationprogramme: nil, identificationregulationpublicationdate: nil, filegenerationdate: nil, identificationvalidfrom: nil, entityeureferencenumber: nil, namealiaslogicalid: nil, addressregulationentryintoforcedate: nil, birthdateregulationorganisationtype: nil, citizenshiplogicalid: nil, identificationcountryiso2code: nil, identificationnumber: nil, addressregulationnumbertitle: nil, citizenshipregulationpublicationurl: nil, birthdateyear: nil, birthdateyearrangefrom: nil, entityregulationtype: nil, identificationregion: nil, entityunitednationid: nil, birthdatemonth: nil, namealiasmiddlename: nil, ...}

    def person_fixture(attrs \\ %{}) do
      {:ok, person} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Persons.create_person()

      person
    end

    test "list_persons/0 returns all persons" do
      person = person_fixture()
      assert Persons.list_persons() == [person]
    end

    test "get_person!/1 returns the person with given id" do
      person = person_fixture()
      assert Persons.get_person!(person.id) == person
    end

    test "create_person/1 with valid data creates a person" do
      assert {:ok, %Person{} = person} = Persons.create_person(@valid_attrs)
      assert person.entitysubjecttypeclassificationcode == "some entitysubjecttypeclassificationcode"
      assert person.namealiasfirstname == "some namealiasfirstname"
      assert person.citizenshipregulationpublicationdate == "some citizenshipregulationpublicationdate"
      assert person.addresslogicalid == "some addresslogicalid"
      assert person.birthdateregulationpublicationdate == "some birthdateregulationpublicationdate"
      assert person.birthdatebirthdate == "some birthdatebirthdate"
      assert person.namealiasregulationentryintoforcedate == "some namealiasregulationentryintoforcedate"
      assert person.entitylogicalid == "some entitylogicalid"
      assert person.identificationregulationlanguage == "some identificationregulationlanguage"
      assert person.birthdateregulationnumbertitle == "some birthdateregulationnumbertitle"
      assert person.entitydesignationdetails == "some entitydesignationdetails"
      assert person.identificationvalidto == "some identificationvalidto"
      assert person.addresscountryiso2code == "some addresscountryiso2code"
      assert person.namealiasregulationlanguage == "some namealiasregulationlanguage"
      assert person.addresszipcode == "some addresszipcode"
      assert person.birthdateplace == "some birthdateplace"
      assert person.identificationrevokedbyissuer == "some identificationrevokedbyissuer"
      assert person.citizenshipregulationprogramme == "some citizenshipregulationprogramme"
      assert person.entitysubjecttype == "some entitysubjecttype"
      assert person.addressregulationprogramme == "some addressregulationprogramme"
      assert person.addresscity == "some addresscity"
      assert person.entityregulationprogramme == "some entityregulationprogramme"
      assert person.birthdateregion == "some birthdateregion"
      assert person.entityregulationentryintoforcedate == "some entityregulationentryintoforcedate"
      assert person.namealiasregulationnumbertitle == "some namealiasregulationnumbertitle"
      assert person.addresspobox == "some addresspobox"
      assert person.identificationknownfalse == "some identificationknownfalse"
      assert person.birthdatezipcode == "some birthdatezipcode"
      assert person.entityregulationnumbertitle == "some entityregulationnumbertitle"
      assert person.identificationregulationnumbertitle == "some identificationregulationnumbertitle"
      assert person.birthdatelogicalid == "some birthdatelogicalid"
      assert person.birthdatecalendartype == "some birthdatecalendartype"
      assert person.citizenshipregulationlanguage == "some citizenshipregulationlanguage"
      assert person.citizenshipregion == "some citizenshipregion"
      assert person.citizenshipcountryiso2code == "some citizenshipcountryiso2code"
      assert person.namealiasremark == "some namealiasremark"
      assert person.namealiasregulationpublicationurl == "some namealiasregulationpublicationurl"
      assert person.birthdateregulationtype == "some birthdateregulationtype"
      assert person.birthdateregulationlanguage == "some birthdateregulationlanguage"
      assert person.namealiasgender == "some namealiasgender"
      assert person.entitydesignationdate == "some entitydesignationdate"
      assert person.namealiasnamelanguage == "some namealiasnamelanguage"
      assert person.addressregion == "some addressregion"
      assert person.identificationregulationtype == "some identificationregulationtype"
      assert person.addressplace == "some addressplace"
      assert person.identificationlogicalid == "some identificationlogicalid"
      assert person.entityregulationorganisationtype == "some entityregulationorganisationtype"
      assert person.identificationcountrydescription == "some identificationcountrydescription"
      assert person.namealiasregulationtype == "some namealiasregulationtype"
      assert person.addressregulationpublicationurl == "some addressregulationpublicationurl"
      assert person.namealiasregulationprogramme == "some namealiasregulationprogramme"
      assert person.birthdatecity == "some birthdatecity"
      assert person.namealiasregulationorganisationtype == "some namealiasregulationorganisationtype"
      assert person.birthdateremark == "some birthdateremark"
      assert person.entityregulationpublicationdate == "some entityregulationpublicationdate"
      assert person.namealiaswholename == "some namealiaswholename"
      assert person.identificationregulationentryintoforcedate == "some identificationregulationentryintoforcedate"
      assert person.identificationregulationorganisationtype == "some identificationregulationorganisationtype"
      assert person.addressregulationorganisationtype == "some addressregulationorganisationtype"
      assert person.identificationregulationpublicationurl == "some identificationregulationpublicationurl"
      assert person.birthdateday == "some birthdateday"
      assert person.addresscontactinfo == "some addresscontactinfo"
      assert person.citizenshipregulationnumbertitle == "some citizenshipregulationnumbertitle"
      assert person.citizenshipcountrydescription == "some citizenshipcountrydescription"
      assert person.identificationreportedlost == "some identificationreportedlost"
      assert person.identificationknownexpired == "some identificationknownexpired"
      assert person.birthdatecountryiso2code == "some birthdatecountryiso2code"
      assert person.identificationremark == "some identificationremark"
      assert person.namealiasmiddlename == "some namealiasmiddlename"
      assert person.birthdatemonth == "some birthdatemonth"
      assert person.entityunitednationid == "some entityunitednationid"
      assert person.identificationregion == "some identificationregion"
      assert person.entityregulationtype == "some entityregulationtype"
      assert person.birthdateyearrangefrom == "some birthdateyearrangefrom"
      assert person.birthdateyear == "some birthdateyear"
      assert person.citizenshipregulationpublicationurl == "some citizenshipregulationpublicationurl"
      assert person.addressregulationnumbertitle == "some addressregulationnumbertitle"
      assert person.identificationnumber == "some identificationnumber"
      assert person.identificationcountryiso2code == "some identificationcountryiso2code"
      assert person.citizenshiplogicalid == "some citizenshiplogicalid"
      assert person.birthdateregulationorganisationtype == "some birthdateregulationorganisationtype"
      assert person.addressregulationentryintoforcedate == "some addressregulationentryintoforcedate"
      assert person.namealiaslogicalid == "some namealiaslogicalid"
      assert person.entityeureferencenumber == "some entityeureferencenumber"
      assert person.identificationvalidfrom == "some identificationvalidfrom"
      assert person.filegenerationdate == "some filegenerationdate"
      assert person.identificationregulationpublicationdate == "some identificationregulationpublicationdate"
      assert person.identificationregulationprogramme == "some identificationregulationprogramme"
      assert person.entityregulationpublicationurl == "some entityregulationpublicationurl"
      assert person.citizenshipregulationtype == "some citizenshipregulationtype"
      assert person.identificationdiplomatic == "some identificationdiplomatic"
      assert person.namealiasfunction == "some namealiasfunction"
      assert person.citizenshipregulationorganisationtype == "some citizenshipregulationorganisationtype"
      assert person.namealiasregulationpublicationdate == "some namealiasregulationpublicationdate"
      assert person.citizenshipremark == "some citizenshipremark"
      assert person.identificationtypecode == "some identificationtypecode"
      assert person.birthdatecirca == "some birthdatecirca"
      assert person.addressremark == "some addressremark"
      assert person.identificationlatinnumber == "some identificationlatinnumber"
      assert person.citizenshipregulationentryintoforcedate == "some citizenshipregulationentryintoforcedate"
      assert person.addressregulationtype == "some addressregulationtype"
      assert person.namealiastitle == "some namealiastitle"
      assert person.identificationtypedescription == "some identificationtypedescription"
      assert person.addressstreet == "some addressstreet"
      assert person.addressregulationlanguage == "some addressregulationlanguage"
      assert person.birthdateregulationentryintoforcedate == "some birthdateregulationentryintoforcedate"
      assert person.birthdateregulationpublicationurl == "some birthdateregulationpublicationurl"
      assert person.birthdatecountrydescription == "some birthdatecountrydescription"
      assert person.identificationissueddate == "some identificationissueddate"
      assert person.addresscountrydescription == "some addresscountrydescription"
      assert person.birthdateyearrangeto == "some birthdateyearrangeto"
      assert person.identificationissuedby == "some identificationissuedby"
      assert person.entityremark == "some entityremark"
      assert person.addressregulationpublicationdate == "some addressregulationpublicationdate"
      assert person.birthdateregulationprogramme == "some birthdateregulationprogramme"
      assert person.addressasatlistingtime == "some addressasatlistingtime"
      assert person.namealiaslastname == "some namealiaslastname"
      assert person.identificationnameondocument == "some identificationnameondocument"
    end

    test "create_person/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Persons.create_person(@invalid_attrs)
    end

    test "update_person/2 with valid data updates the person" do
      person = person_fixture()
      assert {:ok, %Person{} = person} = Persons.update_person(person, @update_attrs)
      assert person.entitysubjecttypeclassificationcode == "some updated entitysubjecttypeclassificationcode"
      assert person.namealiasfirstname == "some updated namealiasfirstname"
      assert person.citizenshipregulationpublicationdate == "some updated citizenshipregulationpublicationdate"
      assert person.addresslogicalid == "some updated addresslogicalid"
      assert person.birthdateregulationpublicationdate == "some updated birthdateregulationpublicationdate"
      assert person.birthdatebirthdate == "some updated birthdatebirthdate"
      assert person.namealiasregulationentryintoforcedate == "some updated namealiasregulationentryintoforcedate"
      assert person.entitylogicalid == "some updated entitylogicalid"
      assert person.identificationregulationlanguage == "some updated identificationregulationlanguage"
      assert person.birthdateregulationnumbertitle == "some updated birthdateregulationnumbertitle"
      assert person.entitydesignationdetails == "some updated entitydesignationdetails"
      assert person.identificationvalidto == "some updated identificationvalidto"
      assert person.addresscountryiso2code == "some updated addresscountryiso2code"
      assert person.namealiasregulationlanguage == "some updated namealiasregulationlanguage"
      assert person.addresszipcode == "some updated addresszipcode"
      assert person.birthdateplace == "some updated birthdateplace"
      assert person.identificationrevokedbyissuer == "some updated identificationrevokedbyissuer"
      assert person.citizenshipregulationprogramme == "some updated citizenshipregulationprogramme"
      assert person.entitysubjecttype == "some updated entitysubjecttype"
      assert person.addressregulationprogramme == "some updated addressregulationprogramme"
      assert person.addresscity == "some updated addresscity"
      assert person.entityregulationprogramme == "some updated entityregulationprogramme"
      assert person.birthdateregion == "some updated birthdateregion"
      assert person.entityregulationentryintoforcedate == "some updated entityregulationentryintoforcedate"
      assert person.namealiasregulationnumbertitle == "some updated namealiasregulationnumbertitle"
      assert person.addresspobox == "some updated addresspobox"
      assert person.identificationknownfalse == "some updated identificationknownfalse"
      assert person.birthdatezipcode == "some updated birthdatezipcode"
      assert person.entityregulationnumbertitle == "some updated entityregulationnumbertitle"
      assert person.identificationregulationnumbertitle == "some updated identificationregulationnumbertitle"
      assert person.birthdatelogicalid == "some updated birthdatelogicalid"
      assert person.birthdatecalendartype == "some updated birthdatecalendartype"
      assert person.citizenshipregulationlanguage == "some updated citizenshipregulationlanguage"
      assert person.citizenshipregion == "some updated citizenshipregion"
      assert person.citizenshipcountryiso2code == "some updated citizenshipcountryiso2code"
      assert person.namealiasremark == "some updated namealiasremark"
      assert person.namealiasregulationpublicationurl == "some updated namealiasregulationpublicationurl"
      assert person.birthdateregulationtype == "some updated birthdateregulationtype"
      assert person.birthdateregulationlanguage == "some updated birthdateregulationlanguage"
      assert person.namealiasgender == "some updated namealiasgender"
      assert person.entitydesignationdate == "some updated entitydesignationdate"
      assert person.namealiasnamelanguage == "some updated namealiasnamelanguage"
      assert person.addressregion == "some updated addressregion"
      assert person.identificationregulationtype == "some updated identificationregulationtype"
      assert person.addressplace == "some updated addressplace"
      assert person.identificationlogicalid == "some updated identificationlogicalid"
      assert person.entityregulationorganisationtype == "some updated entityregulationorganisationtype"
      assert person.identificationcountrydescription == "some updated identificationcountrydescription"
      assert person.namealiasregulationtype == "some updated namealiasregulationtype"
      assert person.addressregulationpublicationurl == "some updated addressregulationpublicationurl"
      assert person.namealiasregulationprogramme == "some updated namealiasregulationprogramme"
      assert person.birthdatecity == "some updated birthdatecity"
      assert person.namealiasregulationorganisationtype == "some updated namealiasregulationorganisationtype"
      assert person.birthdateremark == "some updated birthdateremark"
      assert person.entityregulationpublicationdate == "some updated entityregulationpublicationdate"
      assert person.namealiaswholename == "some updated namealiaswholename"
      assert person.identificationregulationentryintoforcedate == "some updated identificationregulationentryintoforcedate"
      assert person.identificationregulationorganisationtype == "some updated identificationregulationorganisationtype"
      assert person.addressregulationorganisationtype == "some updated addressregulationorganisationtype"
      assert person.identificationregulationpublicationurl == "some updated identificationregulationpublicationurl"
      assert person.birthdateday == "some updated birthdateday"
      assert person.addresscontactinfo == "some updated addresscontactinfo"
      assert person.citizenshipregulationnumbertitle == "some updated citizenshipregulationnumbertitle"
      assert person.citizenshipcountrydescription == "some updated citizenshipcountrydescription"
      assert person.identificationreportedlost == "some updated identificationreportedlost"
      assert person.identificationknownexpired == "some updated identificationknownexpired"
      assert person.birthdatecountryiso2code == "some updated birthdatecountryiso2code"
      assert person.identificationremark == "some updated identificationremark"
      assert person.namealiasmiddlename == "some updated namealiasmiddlename"
      assert person.birthdatemonth == "some updated birthdatemonth"
      assert person.entityunitednationid == "some updated entityunitednationid"
      assert person.identificationregion == "some updated identificationregion"
      assert person.entityregulationtype == "some updated entityregulationtype"
      assert person.birthdateyearrangefrom == "some updated birthdateyearrangefrom"
      assert person.birthdateyear == "some updated birthdateyear"
      assert person.citizenshipregulationpublicationurl == "some updated citizenshipregulationpublicationurl"
      assert person.addressregulationnumbertitle == "some updated addressregulationnumbertitle"
      assert person.identificationnumber == "some updated identificationnumber"
      assert person.identificationcountryiso2code == "some updated identificationcountryiso2code"
      assert person.citizenshiplogicalid == "some updated citizenshiplogicalid"
      assert person.birthdateregulationorganisationtype == "some updated birthdateregulationorganisationtype"
      assert person.addressregulationentryintoforcedate == "some updated addressregulationentryintoforcedate"
      assert person.namealiaslogicalid == "some updated namealiaslogicalid"
      assert person.entityeureferencenumber == "some updated entityeureferencenumber"
      assert person.identificationvalidfrom == "some updated identificationvalidfrom"
      assert person.filegenerationdate == "some updated filegenerationdate"
      assert person.identificationregulationpublicationdate == "some updated identificationregulationpublicationdate"
      assert person.identificationregulationprogramme == "some updated identificationregulationprogramme"
      assert person.entityregulationpublicationurl == "some updated entityregulationpublicationurl"
      assert person.citizenshipregulationtype == "some updated citizenshipregulationtype"
      assert person.identificationdiplomatic == "some updated identificationdiplomatic"
      assert person.namealiasfunction == "some updated namealiasfunction"
      assert person.citizenshipregulationorganisationtype == "some updated citizenshipregulationorganisationtype"
      assert person.namealiasregulationpublicationdate == "some updated namealiasregulationpublicationdate"
      assert person.citizenshipremark == "some updated citizenshipremark"
      assert person.identificationtypecode == "some updated identificationtypecode"
      assert person.birthdatecirca == "some updated birthdatecirca"
      assert person.addressremark == "some updated addressremark"
      assert person.identificationlatinnumber == "some updated identificationlatinnumber"
      assert person.citizenshipregulationentryintoforcedate == "some updated citizenshipregulationentryintoforcedate"
      assert person.addressregulationtype == "some updated addressregulationtype"
      assert person.namealiastitle == "some updated namealiastitle"
      assert person.identificationtypedescription == "some updated identificationtypedescription"
      assert person.addressstreet == "some updated addressstreet"
      assert person.addressregulationlanguage == "some updated addressregulationlanguage"
      assert person.birthdateregulationentryintoforcedate == "some updated birthdateregulationentryintoforcedate"
      assert person.birthdateregulationpublicationurl == "some updated birthdateregulationpublicationurl"
      assert person.birthdatecountrydescription == "some updated birthdatecountrydescription"
      assert person.identificationissueddate == "some updated identificationissueddate"
      assert person.addresscountrydescription == "some updated addresscountrydescription"
      assert person.birthdateyearrangeto == "some updated birthdateyearrangeto"
      assert person.identificationissuedby == "some updated identificationissuedby"
      assert person.entityremark == "some updated entityremark"
      assert person.addressregulationpublicationdate == "some updated addressregulationpublicationdate"
      assert person.birthdateregulationprogramme == "some updated birthdateregulationprogramme"
      assert person.addressasatlistingtime == "some updated addressasatlistingtime"
      assert person.namealiaslastname == "some updated namealiaslastname"
      assert person.identificationnameondocument == "some updated identificationnameondocument"
    end

    test "update_person/2 with invalid data returns error changeset" do
      person = person_fixture()
      assert {:error, %Ecto.Changeset{}} = Persons.update_person(person, @invalid_attrs)
      assert person == Persons.get_person!(person.id)
    end

    test "delete_person/1 deletes the person" do
      person = person_fixture()
      assert {:ok, %Person{}} = Persons.delete_person(person)
      assert_raise Ecto.NoResultsError, fn -> Persons.get_person!(person.id) end
    end

    test "change_person/1 returns a person changeset" do
      person = person_fixture()
      assert %Ecto.Changeset{} = Persons.change_person(person)
    end
  end
end
