defmodule Sanctions.PersonsEUTest do
  use Sanctions.DataCase

  alias Sanctions.PersonsEU

  describe "personseu" do
    alias Sanctions.PersonsEU.PersonEU

    @valid_attrs %{entitylogicalid: "some entitylogicalid", identificationregulationlanguage: "some identificationregulationlanguage", namealiasregulationtype: "some namealiasregulationtype", citizenshipcountryiso2code: "some citizenshipcountryiso2code", entityregulationtype: "some entityregulationtype", filegenerationdate: "some filegenerationdate", identificationtypedescription: "some identificationtypedescription", namealiasregulationorganisationtype: "some namealiasregulationorganisationtype", identificationregulationnumbertitle: "some identificationregulationnumbertitle", addressregulationentryintoforcedate: "some addressregulationentryintoforcedate", entityregulationprogramme: "some entityregulationprogramme", entitydesignationdetails: "some entitydesignationdetails", addressregulationpublicationdate: "some addressregulationpublicationdate", identificationissuedby: "some identificationissuedby", birthdateday: "some birthdateday", namealiasfirstname: "some namealiasfirstname", identificationnumber: "some identificationnumber", namealiaslogicalid: "some namealiaslogicalid", namealiasregulationentryintoforcedate: "some namealiasregulationentryintoforcedate", addressregion: "some addressregion", entityregulationorganisationtype: "some entityregulationorganisationtype", birthdateremark: "some birthdateremark", identificationregulationentryintoforcedate: "some identificationregulationentryintoforcedate", entitydesignationdate: "some entitydesignationdate", entityremark: "some entityremark", addressasatlistingtime: "some addressasatlistingtime", identificationcountryiso2code: "some identificationcountryiso2code", birthdateregulationorganisationtype: "some birthdateregulationorganisationtype", birthdatebirthdate: "some birthdatebirthdate", addressregulationlanguage: "some addressregulationlanguage", addresscontactinfo: "some addresscontactinfo", citizenshipregulationnumbertitle: "some citizenshipregulationnumbertitle", citizenshipregulationentryintoforcedate: "some citizenshipregulationentryintoforcedate", addressregulationorganisationtype: "some addressregulationorganisationtype", identificationnameondocument: "some identificationnameondocument", citizenshiplogicalid: "some citizenshiplogicalid", birthdateregulationprogramme: "some birthdateregulationprogramme", addressregulationpublicationurl: "some addressregulationpublicationurl", citizenshipcountrydescription: "some citizenshipcountrydescription", entityregulationentryintoforcedate: "some entityregulationentryintoforcedate", identificationlogicalid: "some identificationlogicalid", namealiasregulationnumbertitle: "some namealiasregulationnumbertitle", birthdatelogicalid: "some birthdatelogicalid", identificationcountrydescription: "some identificationcountrydescription", birthdateregulationpublicationurl: "some birthdateregulationpublicationurl", identificationlatinnumber: "some identificationlatinnumber", identificationregulationpublicationdate: "some identificationregulationpublicationdate", namealiasgender: "some namealiasgender", birthdatemonth: "some birthdatemonth", birthdatecity: "some birthdatecity", ...}
    @update_attrs %{entitylogicalid: "some updated entitylogicalid", identificationregulationlanguage: "some updated identificationregulationlanguage", namealiasregulationtype: "some updated namealiasregulationtype", citizenshipcountryiso2code: "some updated citizenshipcountryiso2code", entityregulationtype: "some updated entityregulationtype", filegenerationdate: "some updated filegenerationdate", identificationtypedescription: "some updated identificationtypedescription", namealiasregulationorganisationtype: "some updated namealiasregulationorganisationtype", identificationregulationnumbertitle: "some updated identificationregulationnumbertitle", addressregulationentryintoforcedate: "some updated addressregulationentryintoforcedate", entityregulationprogramme: "some updated entityregulationprogramme", entitydesignationdetails: "some updated entitydesignationdetails", addressregulationpublicationdate: "some updated addressregulationpublicationdate", identificationissuedby: "some updated identificationissuedby", birthdateday: "some updated birthdateday", namealiasfirstname: "some updated namealiasfirstname", identificationnumber: "some updated identificationnumber", namealiaslogicalid: "some updated namealiaslogicalid", namealiasregulationentryintoforcedate: "some updated namealiasregulationentryintoforcedate", addressregion: "some updated addressregion", entityregulationorganisationtype: "some updated entityregulationorganisationtype", birthdateremark: "some updated birthdateremark", identificationregulationentryintoforcedate: "some updated identificationregulationentryintoforcedate", entitydesignationdate: "some updated entitydesignationdate", entityremark: "some updated entityremark", addressasatlistingtime: "some updated addressasatlistingtime", identificationcountryiso2code: "some updated identificationcountryiso2code", birthdateregulationorganisationtype: "some updated birthdateregulationorganisationtype", birthdatebirthdate: "some updated birthdatebirthdate", addressregulationlanguage: "some updated addressregulationlanguage", addresscontactinfo: "some updated addresscontactinfo", citizenshipregulationnumbertitle: "some updated citizenshipregulationnumbertitle", citizenshipregulationentryintoforcedate: "some updated citizenshipregulationentryintoforcedate", addressregulationorganisationtype: "some updated addressregulationorganisationtype", identificationnameondocument: "some updated identificationnameondocument", citizenshiplogicalid: "some updated citizenshiplogicalid", birthdateregulationprogramme: "some updated birthdateregulationprogramme", addressregulationpublicationurl: "some updated addressregulationpublicationurl", citizenshipcountrydescription: "some updated citizenshipcountrydescription", entityregulationentryintoforcedate: "some updated entityregulationentryintoforcedate", identificationlogicalid: "some updated identificationlogicalid", namealiasregulationnumbertitle: "some updated namealiasregulationnumbertitle", birthdatelogicalid: "some updated birthdatelogicalid", identificationcountrydescription: "some updated identificationcountrydescription", birthdateregulationpublicationurl: "some updated birthdateregulationpublicationurl", identificationlatinnumber: "some updated identificationlatinnumber", identificationregulationpublicationdate: "some updated identificationregulationpublicationdate", namealiasgender: "some updated namealiasgender", birthdatemonth: "some updated birthdatemonth", birthdatecity: "some updated birthdatecity", ...}
    @invalid_attrs %{entitylogicalid: nil, identificationregulationlanguage: nil, namealiasregulationtype: nil, citizenshipcountryiso2code: nil, entityregulationtype: nil, filegenerationdate: nil, identificationtypedescription: nil, namealiasregulationorganisationtype: nil, identificationregulationnumbertitle: nil, addressregulationentryintoforcedate: nil, entityregulationprogramme: nil, entitydesignationdetails: nil, addressregulationpublicationdate: nil, identificationissuedby: nil, birthdateday: nil, namealiasfirstname: nil, identificationnumber: nil, namealiaslogicalid: nil, namealiasregulationentryintoforcedate: nil, addressregion: nil, entityregulationorganisationtype: nil, birthdateremark: nil, identificationregulationentryintoforcedate: nil, entitydesignationdate: nil, entityremark: nil, addressasatlistingtime: nil, identificationcountryiso2code: nil, birthdateregulationorganisationtype: nil, birthdatebirthdate: nil, addressregulationlanguage: nil, addresscontactinfo: nil, citizenshipregulationnumbertitle: nil, citizenshipregulationentryintoforcedate: nil, addressregulationorganisationtype: nil, identificationnameondocument: nil, citizenshiplogicalid: nil, birthdateregulationprogramme: nil, addressregulationpublicationurl: nil, citizenshipcountrydescription: nil, entityregulationentryintoforcedate: nil, identificationlogicalid: nil, namealiasregulationnumbertitle: nil, birthdatelogicalid: nil, identificationcountrydescription: nil, birthdateregulationpublicationurl: nil, identificationlatinnumber: nil, identificationregulationpublicationdate: nil, namealiasgender: nil, birthdatemonth: nil, birthdatecity: nil, ...}

    def person_eu_fixture(attrs \\ %{}) do
      {:ok, person_eu} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PersonsEU.create_person_eu()

      person_eu
    end

    test "list_personseu/0 returns all personseu" do
      person_eu = person_eu_fixture()
      assert PersonsEU.list_personseu() == [person_eu]
    end

    test "get_person_eu!/1 returns the person_eu with given id" do
      person_eu = person_eu_fixture()
      assert PersonsEU.get_person_eu!(person_eu.id) == person_eu
    end

    test "create_person_eu/1 with valid data creates a person_eu" do
      assert {:ok, %PersonEU{} = person_eu} = PersonsEU.create_person_eu(@valid_attrs)
      assert person_eu.identificationvalidfrom == "some identificationvalidfrom"
      assert person_eu.identificationdiplomatic == "some identificationdiplomatic"
      assert person_eu.namealiasmiddlename == "some namealiasmiddlename"
      assert person_eu.birthdateyearrangeto == "some birthdateyearrangeto"
      assert person_eu.entityeureferencenumber == "some entityeureferencenumber"
      assert person_eu.entitysubjecttype == "some entitysubjecttype"
      assert person_eu.addressregulationtype == "some addressregulationtype"
      assert person_eu.entityregulationpublicationdate == "some entityregulationpublicationdate"
      assert person_eu.addresscountryiso2code == "some addresscountryiso2code"
      assert person_eu.identificationregulationorganisationtype == "some identificationregulationorganisationtype"
      assert person_eu.identificationvalidto == "some identificationvalidto"
      assert person_eu.birthdateplace == "some birthdateplace"
      assert person_eu.addresslogicalid == "some addresslogicalid"
      assert person_eu.identificationregulationpublicationurl == "some identificationregulationpublicationurl"
      assert person_eu.birthdateregulationtype == "some birthdateregulationtype"
      assert person_eu.citizenshipregulationorganisationtype == "some citizenshipregulationorganisationtype"
      assert person_eu.namealiasregulationpublicationdate == "some namealiasregulationpublicationdate"
      assert person_eu.entityregulationpublicationurl == "some entityregulationpublicationurl"
      assert person_eu.birthdateregulationpublicationdate == "some birthdateregulationpublicationdate"
      assert person_eu.birthdatezipcode == "some birthdatezipcode"
      assert person_eu.citizenshipregulationprogramme == "some citizenshipregulationprogramme"
      assert person_eu.birthdatecountryiso2code == "some birthdatecountryiso2code"
      assert person_eu.namealiasregulationprogramme == "some namealiasregulationprogramme"
      assert person_eu.citizenshipregulationpublicationurl == "some citizenshipregulationpublicationurl"
      assert person_eu.addressregulationprogramme == "some addressregulationprogramme"
      assert person_eu.identificationrevokedbyissuer == "some identificationrevokedbyissuer"
      assert person_eu.namealiasfunction == "some namealiasfunction"
      assert person_eu.addresspobox == "some addresspobox"
      assert person_eu.identificationregulationtype == "some identificationregulationtype"
      assert person_eu.identificationtypecode == "some identificationtypecode"
      assert person_eu.identificationknownfalse == "some identificationknownfalse"
      assert person_eu.namealiaslastname == "some namealiaslastname"
      assert person_eu.citizenshipregulationtype == "some citizenshipregulationtype"
      assert person_eu.birthdateregulationnumbertitle == "some birthdateregulationnumbertitle"
      assert person_eu.addressregulationnumbertitle == "some addressregulationnumbertitle"
      assert person_eu.entityunitednationid == "some entityunitednationid"
      assert person_eu.namealiasregulationlanguage == "some namealiasregulationlanguage"
      assert person_eu.namealiaswholename == "some namealiaswholename"
      assert person_eu.addresscity == "some addresscity"
      assert person_eu.identificationregulationprogramme == "some identificationregulationprogramme"
      assert person_eu.birthdateregion == "some birthdateregion"
      assert person_eu.addressstreet == "some addressstreet"
      assert person_eu.citizenshipregulationlanguage == "some citizenshipregulationlanguage"
      assert person_eu.addressremark == "some addressremark"
      assert person_eu.identificationremark == "some identificationremark"
      assert person_eu.identificationissueddate == "some identificationissueddate"
      assert person_eu.citizenshipregion == "some citizenshipregion"
      assert person_eu.entitysubjecttypeclassificationcode == "some entitysubjecttypeclassificationcode"
      assert person_eu.namealiastitle == "some namealiastitle"
      assert person_eu.birthdateregulationentryintoforcedate == "some birthdateregulationentryintoforcedate"
      assert person_eu.namealiasregulationpublicationurl == "some namealiasregulationpublicationurl"
      assert person_eu.birthdateyear == "some birthdateyear"
      assert person_eu.addresszipcode == "some addresszipcode"
      assert person_eu.namealiasremark == "some namealiasremark"
      assert person_eu.namealiasnamelanguage == "some namealiasnamelanguage"
      assert person_eu.identificationreportedlost == "some identificationreportedlost"
      assert person_eu.birthdateregulationlanguage == "some birthdateregulationlanguage"
      assert person_eu.citizenshipregulationpublicationdate == "some citizenshipregulationpublicationdate"
      assert person_eu.birthdatecalendartype == "some birthdatecalendartype"
      assert person_eu.addresscountrydescription == "some addresscountrydescription"
      assert person_eu.entityregulationnumbertitle == "some entityregulationnumbertitle"
      assert person_eu.birthdateyearrangefrom == "some birthdateyearrangefrom"
      assert person_eu.birthdatecirca == "some birthdatecirca"
      assert person_eu.identificationregion == "some identificationregion"
      assert person_eu.citizenshipremark == "some citizenshipremark"
      assert person_eu.identificationknownexpired == "some identificationknownexpired"
      assert person_eu.addressplace == "some addressplace"
      assert person_eu.birthdatecountrydescription == "some birthdatecountrydescription"
      assert person_eu.birthdatecity == "some birthdatecity"
      assert person_eu.birthdatemonth == "some birthdatemonth"
      assert person_eu.namealiasgender == "some namealiasgender"
      assert person_eu.identificationregulationpublicationdate == "some identificationregulationpublicationdate"
      assert person_eu.identificationlatinnumber == "some identificationlatinnumber"
      assert person_eu.birthdateregulationpublicationurl == "some birthdateregulationpublicationurl"
      assert person_eu.identificationcountrydescription == "some identificationcountrydescription"
      assert person_eu.birthdatelogicalid == "some birthdatelogicalid"
      assert person_eu.namealiasregulationnumbertitle == "some namealiasregulationnumbertitle"
      assert person_eu.identificationlogicalid == "some identificationlogicalid"
      assert person_eu.entityregulationentryintoforcedate == "some entityregulationentryintoforcedate"
      assert person_eu.citizenshipcountrydescription == "some citizenshipcountrydescription"
      assert person_eu.addressregulationpublicationurl == "some addressregulationpublicationurl"
      assert person_eu.birthdateregulationprogramme == "some birthdateregulationprogramme"
      assert person_eu.citizenshiplogicalid == "some citizenshiplogicalid"
      assert person_eu.identificationnameondocument == "some identificationnameondocument"
      assert person_eu.addressregulationorganisationtype == "some addressregulationorganisationtype"
      assert person_eu.citizenshipregulationentryintoforcedate == "some citizenshipregulationentryintoforcedate"
      assert person_eu.citizenshipregulationnumbertitle == "some citizenshipregulationnumbertitle"
      assert person_eu.addresscontactinfo == "some addresscontactinfo"
      assert person_eu.addressregulationlanguage == "some addressregulationlanguage"
      assert person_eu.birthdatebirthdate == "some birthdatebirthdate"
      assert person_eu.birthdateregulationorganisationtype == "some birthdateregulationorganisationtype"
      assert person_eu.identificationcountryiso2code == "some identificationcountryiso2code"
      assert person_eu.addressasatlistingtime == "some addressasatlistingtime"
      assert person_eu.entityremark == "some entityremark"
      assert person_eu.entitydesignationdate == "some entitydesignationdate"
      assert person_eu.identificationregulationentryintoforcedate == "some identificationregulationentryintoforcedate"
      assert person_eu.birthdateremark == "some birthdateremark"
      assert person_eu.entityregulationorganisationtype == "some entityregulationorganisationtype"
      assert person_eu.addressregion == "some addressregion"
      assert person_eu.namealiasregulationentryintoforcedate == "some namealiasregulationentryintoforcedate"
      assert person_eu.namealiaslogicalid == "some namealiaslogicalid"
      assert person_eu.identificationnumber == "some identificationnumber"
      assert person_eu.namealiasfirstname == "some namealiasfirstname"
      assert person_eu.birthdateday == "some birthdateday"
      assert person_eu.identificationissuedby == "some identificationissuedby"
      assert person_eu.addressregulationpublicationdate == "some addressregulationpublicationdate"
      assert person_eu.entitydesignationdetails == "some entitydesignationdetails"
      assert person_eu.entityregulationprogramme == "some entityregulationprogramme"
      assert person_eu.addressregulationentryintoforcedate == "some addressregulationentryintoforcedate"
      assert person_eu.identificationregulationnumbertitle == "some identificationregulationnumbertitle"
      assert person_eu.namealiasregulationorganisationtype == "some namealiasregulationorganisationtype"
      assert person_eu.identificationtypedescription == "some identificationtypedescription"
      assert person_eu.filegenerationdate == "some filegenerationdate"
      assert person_eu.entityregulationtype == "some entityregulationtype"
      assert person_eu.citizenshipcountryiso2code == "some citizenshipcountryiso2code"
      assert person_eu.namealiasregulationtype == "some namealiasregulationtype"
      assert person_eu.identificationregulationlanguage == "some identificationregulationlanguage"
      assert person_eu.entitylogicalid == "some entitylogicalid"
    end

    test "create_person_eu/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PersonsEU.create_person_eu(@invalid_attrs)
    end

    test "update_person_eu/2 with valid data updates the person_eu" do
      person_eu = person_eu_fixture()
      assert {:ok, %PersonEU{} = person_eu} = PersonsEU.update_person_eu(person_eu, @update_attrs)
      assert person_eu.identificationvalidfrom == "some updated identificationvalidfrom"
      assert person_eu.identificationdiplomatic == "some updated identificationdiplomatic"
      assert person_eu.namealiasmiddlename == "some updated namealiasmiddlename"
      assert person_eu.birthdateyearrangeto == "some updated birthdateyearrangeto"
      assert person_eu.entityeureferencenumber == "some updated entityeureferencenumber"
      assert person_eu.entitysubjecttype == "some updated entitysubjecttype"
      assert person_eu.addressregulationtype == "some updated addressregulationtype"
      assert person_eu.entityregulationpublicationdate == "some updated entityregulationpublicationdate"
      assert person_eu.addresscountryiso2code == "some updated addresscountryiso2code"
      assert person_eu.identificationregulationorganisationtype == "some updated identificationregulationorganisationtype"
      assert person_eu.identificationvalidto == "some updated identificationvalidto"
      assert person_eu.birthdateplace == "some updated birthdateplace"
      assert person_eu.addresslogicalid == "some updated addresslogicalid"
      assert person_eu.identificationregulationpublicationurl == "some updated identificationregulationpublicationurl"
      assert person_eu.birthdateregulationtype == "some updated birthdateregulationtype"
      assert person_eu.citizenshipregulationorganisationtype == "some updated citizenshipregulationorganisationtype"
      assert person_eu.namealiasregulationpublicationdate == "some updated namealiasregulationpublicationdate"
      assert person_eu.entityregulationpublicationurl == "some updated entityregulationpublicationurl"
      assert person_eu.birthdateregulationpublicationdate == "some updated birthdateregulationpublicationdate"
      assert person_eu.birthdatezipcode == "some updated birthdatezipcode"
      assert person_eu.citizenshipregulationprogramme == "some updated citizenshipregulationprogramme"
      assert person_eu.birthdatecountryiso2code == "some updated birthdatecountryiso2code"
      assert person_eu.namealiasregulationprogramme == "some updated namealiasregulationprogramme"
      assert person_eu.citizenshipregulationpublicationurl == "some updated citizenshipregulationpublicationurl"
      assert person_eu.addressregulationprogramme == "some updated addressregulationprogramme"
      assert person_eu.identificationrevokedbyissuer == "some updated identificationrevokedbyissuer"
      assert person_eu.namealiasfunction == "some updated namealiasfunction"
      assert person_eu.addresspobox == "some updated addresspobox"
      assert person_eu.identificationregulationtype == "some updated identificationregulationtype"
      assert person_eu.identificationtypecode == "some updated identificationtypecode"
      assert person_eu.identificationknownfalse == "some updated identificationknownfalse"
      assert person_eu.namealiaslastname == "some updated namealiaslastname"
      assert person_eu.citizenshipregulationtype == "some updated citizenshipregulationtype"
      assert person_eu.birthdateregulationnumbertitle == "some updated birthdateregulationnumbertitle"
      assert person_eu.addressregulationnumbertitle == "some updated addressregulationnumbertitle"
      assert person_eu.entityunitednationid == "some updated entityunitednationid"
      assert person_eu.namealiasregulationlanguage == "some updated namealiasregulationlanguage"
      assert person_eu.namealiaswholename == "some updated namealiaswholename"
      assert person_eu.addresscity == "some updated addresscity"
      assert person_eu.identificationregulationprogramme == "some updated identificationregulationprogramme"
      assert person_eu.birthdateregion == "some updated birthdateregion"
      assert person_eu.addressstreet == "some updated addressstreet"
      assert person_eu.citizenshipregulationlanguage == "some updated citizenshipregulationlanguage"
      assert person_eu.addressremark == "some updated addressremark"
      assert person_eu.identificationremark == "some updated identificationremark"
      assert person_eu.identificationissueddate == "some updated identificationissueddate"
      assert person_eu.citizenshipregion == "some updated citizenshipregion"
      assert person_eu.entitysubjecttypeclassificationcode == "some updated entitysubjecttypeclassificationcode"
      assert person_eu.namealiastitle == "some updated namealiastitle"
      assert person_eu.birthdateregulationentryintoforcedate == "some updated birthdateregulationentryintoforcedate"
      assert person_eu.namealiasregulationpublicationurl == "some updated namealiasregulationpublicationurl"
      assert person_eu.birthdateyear == "some updated birthdateyear"
      assert person_eu.addresszipcode == "some updated addresszipcode"
      assert person_eu.namealiasremark == "some updated namealiasremark"
      assert person_eu.namealiasnamelanguage == "some updated namealiasnamelanguage"
      assert person_eu.identificationreportedlost == "some updated identificationreportedlost"
      assert person_eu.birthdateregulationlanguage == "some updated birthdateregulationlanguage"
      assert person_eu.citizenshipregulationpublicationdate == "some updated citizenshipregulationpublicationdate"
      assert person_eu.birthdatecalendartype == "some updated birthdatecalendartype"
      assert person_eu.addresscountrydescription == "some updated addresscountrydescription"
      assert person_eu.entityregulationnumbertitle == "some updated entityregulationnumbertitle"
      assert person_eu.birthdateyearrangefrom == "some updated birthdateyearrangefrom"
      assert person_eu.birthdatecirca == "some updated birthdatecirca"
      assert person_eu.identificationregion == "some updated identificationregion"
      assert person_eu.citizenshipremark == "some updated citizenshipremark"
      assert person_eu.identificationknownexpired == "some updated identificationknownexpired"
      assert person_eu.addressplace == "some updated addressplace"
      assert person_eu.birthdatecountrydescription == "some updated birthdatecountrydescription"
      assert person_eu.birthdatecity == "some updated birthdatecity"
      assert person_eu.birthdatemonth == "some updated birthdatemonth"
      assert person_eu.namealiasgender == "some updated namealiasgender"
      assert person_eu.identificationregulationpublicationdate == "some updated identificationregulationpublicationdate"
      assert person_eu.identificationlatinnumber == "some updated identificationlatinnumber"
      assert person_eu.birthdateregulationpublicationurl == "some updated birthdateregulationpublicationurl"
      assert person_eu.identificationcountrydescription == "some updated identificationcountrydescription"
      assert person_eu.birthdatelogicalid == "some updated birthdatelogicalid"
      assert person_eu.namealiasregulationnumbertitle == "some updated namealiasregulationnumbertitle"
      assert person_eu.identificationlogicalid == "some updated identificationlogicalid"
      assert person_eu.entityregulationentryintoforcedate == "some updated entityregulationentryintoforcedate"
      assert person_eu.citizenshipcountrydescription == "some updated citizenshipcountrydescription"
      assert person_eu.addressregulationpublicationurl == "some updated addressregulationpublicationurl"
      assert person_eu.birthdateregulationprogramme == "some updated birthdateregulationprogramme"
      assert person_eu.citizenshiplogicalid == "some updated citizenshiplogicalid"
      assert person_eu.identificationnameondocument == "some updated identificationnameondocument"
      assert person_eu.addressregulationorganisationtype == "some updated addressregulationorganisationtype"
      assert person_eu.citizenshipregulationentryintoforcedate == "some updated citizenshipregulationentryintoforcedate"
      assert person_eu.citizenshipregulationnumbertitle == "some updated citizenshipregulationnumbertitle"
      assert person_eu.addresscontactinfo == "some updated addresscontactinfo"
      assert person_eu.addressregulationlanguage == "some updated addressregulationlanguage"
      assert person_eu.birthdatebirthdate == "some updated birthdatebirthdate"
      assert person_eu.birthdateregulationorganisationtype == "some updated birthdateregulationorganisationtype"
      assert person_eu.identificationcountryiso2code == "some updated identificationcountryiso2code"
      assert person_eu.addressasatlistingtime == "some updated addressasatlistingtime"
      assert person_eu.entityremark == "some updated entityremark"
      assert person_eu.entitydesignationdate == "some updated entitydesignationdate"
      assert person_eu.identificationregulationentryintoforcedate == "some updated identificationregulationentryintoforcedate"
      assert person_eu.birthdateremark == "some updated birthdateremark"
      assert person_eu.entityregulationorganisationtype == "some updated entityregulationorganisationtype"
      assert person_eu.addressregion == "some updated addressregion"
      assert person_eu.namealiasregulationentryintoforcedate == "some updated namealiasregulationentryintoforcedate"
      assert person_eu.namealiaslogicalid == "some updated namealiaslogicalid"
      assert person_eu.identificationnumber == "some updated identificationnumber"
      assert person_eu.namealiasfirstname == "some updated namealiasfirstname"
      assert person_eu.birthdateday == "some updated birthdateday"
      assert person_eu.identificationissuedby == "some updated identificationissuedby"
      assert person_eu.addressregulationpublicationdate == "some updated addressregulationpublicationdate"
      assert person_eu.entitydesignationdetails == "some updated entitydesignationdetails"
      assert person_eu.entityregulationprogramme == "some updated entityregulationprogramme"
      assert person_eu.addressregulationentryintoforcedate == "some updated addressregulationentryintoforcedate"
      assert person_eu.identificationregulationnumbertitle == "some updated identificationregulationnumbertitle"
      assert person_eu.namealiasregulationorganisationtype == "some updated namealiasregulationorganisationtype"
      assert person_eu.identificationtypedescription == "some updated identificationtypedescription"
      assert person_eu.filegenerationdate == "some updated filegenerationdate"
      assert person_eu.entityregulationtype == "some updated entityregulationtype"
      assert person_eu.citizenshipcountryiso2code == "some updated citizenshipcountryiso2code"
      assert person_eu.namealiasregulationtype == "some updated namealiasregulationtype"
      assert person_eu.identificationregulationlanguage == "some updated identificationregulationlanguage"
      assert person_eu.entitylogicalid == "some updated entitylogicalid"
    end

    test "update_person_eu/2 with invalid data returns error changeset" do
      person_eu = person_eu_fixture()
      assert {:error, %Ecto.Changeset{}} = PersonsEU.update_person_eu(person_eu, @invalid_attrs)
      assert person_eu == PersonsEU.get_person_eu!(person_eu.id)
    end

    test "delete_person_eu/1 deletes the person_eu" do
      person_eu = person_eu_fixture()
      assert {:ok, %PersonEU{}} = PersonsEU.delete_person_eu(person_eu)
      assert_raise Ecto.NoResultsError, fn -> PersonsEU.get_person_eu!(person_eu.id) end
    end

    test "change_person_eu/1 returns a person_eu changeset" do
      person_eu = person_eu_fixture()
      assert %Ecto.Changeset{} = PersonsEU.change_person_eu(person_eu)
    end
  end
end
