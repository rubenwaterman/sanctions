# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :sanctions,
  ecto_repos: [Sanctions.Repo]

# Configures the endpoint
config :sanctions, SanctionsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SZwvcaR3SKPQgFUHi1Be5MtTJzdkqTrMxbuNQuDuA4hhLW1mbQT5spBxI2d4/ub5",
  render_errors: [view: SanctionsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Sanctions.PubSub,
  live_view: [signing_salt: "jto0fV3z"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :sanctions, Sanctions.Mailer,
  adapter: Bamboo.MandrillAdapter,
  api_key: "my_api_key"

config :kaffy,
   otp_app: :sanctions,
   ecto_repo: Sanctions.Repo,
   router: SanctionsWeb.Router

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
